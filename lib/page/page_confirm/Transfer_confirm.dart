// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myapp/page/low_bar/lowbar.dart';
import 'package:myapp/page/Homepage.dart';
class Transfer_confirm extends StatefulWidget {
  String qrCodeResult;
  Transfer_confirm({Key? key, required this.qrCodeResult});

  @override
  State<Transfer_confirm> createState() => _Transfer_confirmState(qrCodeResult);
}

class _Transfer_confirmState extends State<Transfer_confirm> {
  var qrCodeResult;

  _Transfer_confirmState(this.qrCodeResult);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(180),
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.account_circle),
            iconSize: 35.0,
            padding: EdgeInsets.all(20.0),
          ),
          actions: [
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.power_settings_new),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
          ],
          flexibleSpace: Container(
            child: Align(
              alignment: Alignment(0, 0.5),
              child: Text(
                'Transfer',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'SegoeUI_Bold',
                  fontSize: 50,
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 179, 0, 0),
                  Color.fromARGB(255, 132, 15, 15),
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomCenter,
              ),
              image: DecorationImage(
                image: AssetImage("assets/mfu_icon.png"),
                alignment: Alignment(0, -0.6),
                scale: 7,
              ),
            ),
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(30),
        children: [
          Text(
            'To: MFU Wallet Account',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              letterSpacing: 2.0,
              color: Colors.grey[850],
              fontFamily: 'Trajan Pro',
            ),
          ),
          SizedBox(height: 10),
          Form(
            child: Column(
              children: [
                SizedBox(height: 20),
                Container(
                  width: 500,
                  height: 35,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        top: 0,
                        left: 50,
                        child: Text(
                          'Name: $qrCodeResult',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontFamily: 'Segoe UI',
                              fontSize: 15,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        ),
                      ),
                      Positioned(
                        top: 20,
                        left: 50,
                        child: Text(
                          'Account No. xxx-x-x3458-x',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(88, 88, 88, 1),
                              fontFamily: 'Segoe UI',
                              fontSize: 10,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        ),
                      ),
                      Positioned(
                        top: 0,
                        left: 0,
                        child: Container(
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/LogoMFU_Wallet.png'),
                                fit: BoxFit.fitWidth),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                buildAmount(),
                SizedBox(height: 120),
                // ignore: deprecated_member_use
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    gradient: LinearGradient(
                      // ignore: prefer_const_literals_to_create_immutables
                      colors: [
                        Color.fromARGB(255, 179, 0, 0),
                        Color.fromARGB(255, 132, 15, 15),
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  height: 45,
                  width: 600,
                  child: OutlinedButton(
                    onPressed: () => showCustomDialog(context),
                    child: Text(
                      'Next',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontFamily: 'Trajan Pro',
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size.fromHeight(80),
                      textStyle: TextStyle(fontSize: 20),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                // ignore: deprecated_member_use
                Container(
                  height: 45,
                  width: 600,
                  child: OutlinedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Home()));
                    },
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Trajan Pro',
                      ),
                    ),
                    style: OutlinedButton.styleFrom(
                      minimumSize: Size.fromHeight(80),
                      textStyle: TextStyle(fontSize: 20),
                      primary: Color.fromARGB(255, 179, 0, 0),
                      side: BorderSide(
                          width: 1, color: Color.fromARGB(255, 179, 0, 0)),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Widget buildAmount() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        '  Amount',
        style: TextStyle(
            color: Color.fromRGBO(88, 88, 88, 1),
            fontSize: 16,
            fontWeight: FontWeight.normal),
      ),
      SizedBox(height: 10),
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            // ignore: prefer_const_literals_to_create_immutables
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
            ]),
        height: 60,
        child: TextField(
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(color: Colors.black87),
          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),
              prefixIcon: Icon(Icons.monetization_on, color: Color(0xff720E16)),
              hintText: '                                      0.00 MFC',
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}

void showCustomDialog(BuildContext context) => showDialog(
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 12),
              Image(
                image: AssetImage("assets/transfer.png"),
                alignment: Alignment(0, -0.6),
                height: 80,
                width: 80,
              ),
              SizedBox(height: 12),
              Text(
                'Oh, You tranfer to this account Are you sure?',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(height: 12),
              Row(
                children: [
                  Padding(padding: EdgeInsets.fromLTRB(7, 0, 0, 0)),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                      ),
                      gradient: LinearGradient(
                        colors: [
                          Color.fromARGB(255, 179, 0, 0),
                          Color.fromARGB(255, 132, 15, 15),
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    height: 40,
                    width: 100,
                    child: OutlinedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text(
                        'GO BACK',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                          fontFamily: 'Trajan Pro',
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size.fromHeight(80),
                        textStyle: TextStyle(fontSize: 15),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.fromLTRB(50, 0, 0, 0)),
                  Container(
                    height: 40,
                    width: 100,
                    child: OutlinedButton(
                      onPressed: () {
                        Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => Home()));
                      },
                      child: Text(
                        'ACCEPT',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 179, 0, 0),
                          fontFamily: 'Trajan Pro',
                        ),
                      ),
                      style: OutlinedButton.styleFrom(
                        minimumSize: Size.fromHeight(80),
                        textStyle: TextStyle(fontSize: 15),
                        primary: Color.fromARGB(255, 179, 0, 0),
                        side: BorderSide(
                            width: 1, color: Color.fromARGB(255, 179, 0, 0)),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 60),
                ],
              )
            ],
          ),
        ),
      ),
      context: context,
      barrierDismissible: false,
    );
