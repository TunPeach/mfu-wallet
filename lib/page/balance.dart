// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/material.dart';

import 'package:myapp/page/page_confirm/Transfer_confirm.dart';
import 'low_bar/lowbar.dart';
import 'Homepage.dart';
import 'low_bar/lowbar_balance.dart';
import 'more.dart';
import 'payment.dart';
import 'withdraw.dart';
import 'Exchange.dart';
import 'History.dart';
class Balance extends StatefulWidget {
  const Balance({Key? key}) : super(key: key);

  @override
  _BalanceState createState() => _BalanceState();
}

class _BalanceState extends State<Balance> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(230),
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.account_circle),
            iconSize: 35.0,
            padding: EdgeInsets.all(20.0),
          ),
          actions: [
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.power_settings_new),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
          ],
          flexibleSpace: Container(
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                
              balanceMFC(),
              SizedBox(width: 50,),
              balanceTHBD(),
              SizedBox(width: 10,),
              ],
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 179, 0, 0),
                  Color.fromARGB(255, 132, 15, 15),
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomCenter,
              ),
              image: DecorationImage(
                image: AssetImage("assets/mfu_icon.png"),
                alignment: Alignment(0, -0.7),
                scale: 7,
              ),
            ),
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(30),
        children: [
          IconButtonMain(),
          Padding(padding: EdgeInsets.all(10)),
          IconButtonMain2(),
          Padding(padding: EdgeInsets.all(20)),
          MyStatelessWidget(),
          Padding(padding: EdgeInsets.all(5)),
          Card2(),
        ],
      ),
    );
  }
}

class MyStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: Colors.red,
      elevation: 8,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 179, 0, 0),
              Color.fromARGB(255, 99, 0, 0)
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'My favorites',
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              'Add favorited transaction to home page ',
              style: TextStyle(
                fontSize: 10,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Card2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: Colors.red,
      elevation: 8,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 179, 0, 0),
              Color.fromARGB(255, 99, 0, 0)
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Quick Balance',
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              'Add account or card to instantily check balance',
              style: TextStyle(
                fontSize: 10,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class IconButtonMain extends StatelessWidget {
  const IconButtonMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Material(
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => lowbar_balance()));
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0.0),
                  child: Image.asset(
                    'assets/deposit.png',
                    scale: 14,
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.all(3)),
            Text(
              "Balance",
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Column(
          children: [
            Material(
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Withdraw()));
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0.0),
                  child: Image.asset(
                    'assets/withdraw.png',
                    scale: 14,
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.all(3)),
            Text("Withdraw",
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ],
        ),
        Column(
          children: [
            Material(
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Exchange()));
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0.0),
                  child: Image.asset(
                    'assets/exchange.png',
                    scale: 14,
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.all(3)),
            Text("Exchange",
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ],
        ),
      ],
    );
  }
}

class IconButtonMain2 extends StatelessWidget {
  const IconButtonMain2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
          child: Column(
            children: [
              Material(
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Payment()));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(0.0),
                    child: Image.asset(
                      'assets/payment.png',
                      scale: 14,
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.all(3)),
              Text(
                "Payment",
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 15, 0),
          child: Column(
            children: [
              Material(
                child: InkWell(
                  onTap: () async {
                    ScanResult codeSanner = await BarcodeScanner.scan();
                    String qrResult = codeSanner.rawContent;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                Transfer_confirm(qrCodeResult: qrResult)));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(0.0),
                    child: Image.asset(
                      'assets/transfer.png',
                      scale: 14,
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.all(3)),
              Text("Transfer",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
          child: Column(
            children: [
              Material(
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => History()));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(0.0),
                    child: Image.asset(
                      'assets/history.png',
                      scale: 14,
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.all(3)),
              Text("History",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
            ],
          ),
        ),
      ],
    );
  }
}

Widget balanceMFC() => 
    Padding(
      padding: const EdgeInsets.fromLTRB(80,0,50,0),
      child: Column(
        children: [
          SizedBox(height: 140),
          Align(
            child: Text(
              'TOTAL BALANCE',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
          Align(
            child: Text(
              '100,000 MFC',
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'SegoeUI_Bold',
                fontSize: 35,
              ),
            ),
          ),
        ],
      ),
    );

    Widget balanceTHBD() => 
    Padding(
      padding: const EdgeInsets.fromLTRB(80,0,40,0),
      child: Column(
        children: [
          SizedBox(height: 140),
          Align(
            child: Text(
              'TOTAL BALANCE',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
          Align(
            child: Text(
              '100,000 THBD',
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'SegoeUI_Bold',
                fontSize: 35,
              ),
            ),
          ),
        ],
      ),
    );


