// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:myapp/page/low_bar/low_barMore.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(180),
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.account_circle),
            iconSize: 35.0,
            padding: EdgeInsets.all(20.0),
          ),
          actions: [
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.power_settings_new),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
          ],
          flexibleSpace: Container(
            child: Align(
              alignment: Alignment(0, 0.5),
              child: Text(
                'Profile',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'SegoeUI_Bold',
                  fontSize: 50,
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 179, 0, 0),
                  Color.fromARGB(255, 132, 15, 15),
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomCenter,
              ),
              image: DecorationImage(
                image: AssetImage("assets/mfu_icon.png"),
                alignment: Alignment(0, -0.6),
                scale: 7,
              ),
            ),
          ),
        ),
      ),
      body: ListView(
        children: [
          card(),
          SizedBox(height: 30,),
          Container(
            height: 45,
            width: 600,
            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: OutlinedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Home_More()));
              },
              child: Text(
                'Back',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Trajan Pro',
                ),
              ),
              style: OutlinedButton.styleFrom(
                minimumSize: Size.fromHeight(80),
                textStyle: TextStyle(fontSize: 20),
                primary: Color.fromARGB(255, 179, 0, 0),
                side:
                    BorderSide(width: 1, color: Color.fromARGB(255, 179, 0, 0)),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget card() {
  return Column(
    children: [
      SizedBox(
        height: 40,
      ),
      cradpro(),
    ],
  );
}

Widget cradpro() {
  return // Figma Flutter Generator Group37Widget - GROUP
      Container(
          width: 335,
          height: 356,
          child: Stack(children: <Widget>[
            Positioned(
                top: 0,
                left: 127,
                child: Container(
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/profile black.png'),
                          fit: BoxFit.fitWidth),
                    ))),
            Positioned(
                top: 151,
                left: 0,
                child: Container(
                    width: 335,
                    height: 205,
                    child: Stack(children: <Widget>[
                      Positioned(
                          top: 156,
                          left: 0,
                          child: Container(
                              width: 335,
                              height: 49,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(0),
                                  topRight: Radius.circular(0),
                                  bottomLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.25),
                                      offset: Offset(6, 6),
                                      blurRadius: 15)
                                ],
                                gradient: LinearGradient(
                                    begin: Alignment(-0.27715936303138733,
                                        -0.4056510329246521),
                                    end: Alignment(0.4056510329246521,
                                        -0.1148015558719635),
                                    colors: [
                                      Color.fromRGBO(112, 17, 5, 1),
                                      Color.fromRGBO(167, 45, 10, 1),
                                    ]),
                              ))),
                      Positioned(
                          top: 0,
                          left: 0,
                          child: Container(
                              width: 335,
                              height: 167,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(0),
                                  bottomRight: Radius.circular(0),
                                ),
                                color: Color.fromRGBO(255, 255, 255, 1),
                              ))),
                      Positioned(
                          top: 55,
                          left: 22,
                          child: Text(
                            'Firstname',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(0, 0, 0, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 15,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 15,
                          left: 21,
                          child: Text(
                            'Username',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(0, 0, 0, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 15,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 92,
                          left: 23,
                          child: Text(
                            'Lastname',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(0, 0, 0, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 15,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 132,
                          left: 24,
                          child: Text(
                            'Email',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(0, 0, 0, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 15,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 62,
                          left: 134,
                          child: Text(
                            'Pitakpong',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(88, 88, 88, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 10,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 98,
                          left: 130,
                          child: Text(
                            ' Nilta',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(88, 88, 88, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 10,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 134,
                          left: 134,
                          child: Text(
                            'peach2345@gmail.com',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(88, 88, 88, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 10,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 180,
                          left: 24,
                          child: Text(
                            'Hash Wallet',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 15,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 182,
                          left: 134,
                          child: Text(
                            'hasdf3eriocp[i3ok-fdhgi26346464werwijr',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 10,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                      Positioned(
                          top: 22,
                          left: 134,
                          child: Text(
                            'TunPeach',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Color.fromRGBO(88, 88, 88, 1),
                                fontFamily: 'Segoe UI',
                                fontSize: 10,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )),
                    ]))),
            Positioned(
                top: 92,
                left: 122,
                child: Text(
                  'TunPeach',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color.fromRGBO(88, 88, 88, 1),
                      fontFamily: 'Segoe UI',
                      fontSize: 20,
                      letterSpacing:
                          0 /*percentages not used in flutter. defaulting to zero*/,
                      fontWeight: FontWeight.normal,
                      height: 1),
                )),
          ]));
}
