// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:myapp/page/low_bar/lowbar.dart';

class Exchange extends StatefulWidget {
  const Exchange({Key? key}) : super(key: key);

  @override
  _ExchangeState createState() => _ExchangeState();
}

class _ExchangeState extends State<Exchange> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(180),
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.account_circle),
            iconSize: 35.0,
            padding: EdgeInsets.all(20.0),
          ),
          actions: [
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.power_settings_new),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
          ],
          flexibleSpace: Container(
            child: Align(
              alignment: Alignment(0, 0.5),
              child: Text(
                'Exchange',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'SegoeUI_Bold',
                  fontSize: 50,
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 179, 0, 0),
                  Color.fromARGB(255, 132, 15, 15)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomCenter,
              ),
              image: DecorationImage(
                image: AssetImage("assets/mfu_icon.png"),
                alignment: Alignment(0, -0.6),
                scale: 7,
              ),
            ),
          ),
        ),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              card(),
              SizedBox(
                height: 100,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                  ),
                  gradient: LinearGradient(
                    // ignore: prefer_const_literals_to_create_immutables
                    colors: [
                      Color.fromARGB(255, 179, 0, 0),
                      Color.fromARGB(255, 132, 15, 15),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomCenter,
                  ),
                ),
                height: 45,
                width: 320,
                child: OutlinedButton(
                  onPressed: () => showCustomDialog(context),
                  child: Text(
                    'Next',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontFamily: 'Trajan Pro',
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size.fromHeight(80),
                    textStyle: TextStyle(fontSize: 20),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
              // ignore: deprecated_member_use
              Container(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                height: 45,
                width: 600,
                child: OutlinedButton(
                  onPressed: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) => Home()));
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Trajan Pro',
                    ),
                  ),
                  style: OutlinedButton.styleFrom(
                    minimumSize: Size.fromHeight(80),
                    textStyle: TextStyle(fontSize: 20),
                    primary: Color.fromARGB(255, 179, 0, 0),
                    side: BorderSide(
                        width: 1, color: Color.fromARGB(255, 179, 0, 0)),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

void showCustomDialog(BuildContext context) => showDialog(
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 12),
              Image(
                image: AssetImage("assets/exchange.png"),
                alignment: Alignment(0, -0.6),
                height: 80,
                width: 80,
              ),
              SizedBox(height: 12),
              Text(
                'Oh, You exchange to this coins Are you sure?',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(height: 12),
              Row(
                children: [
                  Padding(padding: EdgeInsets.fromLTRB(7, 0, 0, 0)),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                      ),
                      gradient: LinearGradient(
                        colors: [
                          Color.fromARGB(255, 179, 0, 0),
                          Color.fromARGB(255, 132, 15, 15),
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    height: 40,
                    width: 100,
                    child: OutlinedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text(
                        'GO BACK',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                          fontFamily: 'Trajan Pro',
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size.fromHeight(80),
                        textStyle: TextStyle(fontSize: 15),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.fromLTRB(50, 0, 0, 0)),
                  Container(
                    height: 40,
                    width: 100,
                    child: OutlinedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => Home()));
                      },
                      child: Text(
                        'ACCEPT',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 179, 0, 0),
                          fontFamily: 'Trajan Pro',
                        ),
                      ),
                      style: OutlinedButton.styleFrom(
                        minimumSize: Size.fromHeight(80),
                        textStyle: TextStyle(fontSize: 15),
                        primary: Color.fromARGB(255, 179, 0, 0),
                        side: BorderSide(
                            width: 1, color: Color.fromARGB(255, 179, 0, 0)),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 60),
                ],
              )
            ],
          ),
        ),
      ),
      context: context,
      barrierDismissible: false,
    );

// Widget buildUsername() {
//   return Column(
//     children: <Widget>[
//       Container(
//           width: 80.0,
//         alignment: Alignment.centerLeft,
//         decoration: BoxDecoration(

//             boxShadow: [
//               BoxShadow(
//                   color: Color.fromARGB(66, 255, 255, 255), blurRadius: 6, offset: Offset(0, 2))
//             ]),
//         child: TextField(

//           keyboardType: TextInputType.number,
//           style: TextStyle(color: Colors.black87, fontSize: 30, ),
//           decoration: InputDecoration(
//               border: InputBorder.none,
//               hintText: '0.00',
//               hintStyle: TextStyle(color: Colors.black38,)

//               ),
//         ),
//       )
//     ],
//   );
// }

Widget card() {
  return Container(
    width: 335,
    height: 250,
    child: Stack(
      children: <Widget>[
        Positioned(
          top: 167,
          left: 0,
          child: Container(
            width: 335,
            height: 85,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0),
                topRight: Radius.circular(0),
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
              boxShadow: [
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.25),
                    offset: Offset(6, 6),
                    blurRadius: 15)
              ],
              gradient: LinearGradient(
                begin: Alignment(-0.27715936303138733, -0.4056510329246521),
                end: Alignment(0.4056510329246521, -0.1148015558719635),
                colors: [
                  Color.fromARGB(255, 132, 15, 15),
                  Color.fromARGB(255, 179, 0, 0),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          top: 186,
          left: 17,
          child: Text(
            'Average MFC Rate',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
                fontFamily: 'Segoe UI',
                fontSize: 15,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        Positioned(
          top: 184,
          left: 190,
          child: Text(
            'Average THBD Rate',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
                fontFamily: 'Segoe UI',
                fontSize: 15,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        Positioned(
          top: 0,
          left: 0,
          child: Container(
            width: 335,
            height: 167,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                bottomLeft: Radius.circular(0),
                bottomRight: Radius.circular(0),
              ),
              color: Color.fromRGBO(255, 255, 255, 1),
            ),
          ),
        ),
        Positioned(
          top: 25,
          left: 33,
          child: Text(
            'EXCHANGE OPERATION',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(0, 0, 0, 1),
                fontFamily: 'Segoe UI',
                fontSize: 15,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        Positioned(
          top: 70,
          left: 43,
          child: Text(
            'Sell MFC',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(0, 0, 0, 1),
                fontFamily: 'Segoe UI',
                fontSize: 20,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        Positioned(
          top: 70,
          left: 220,
          child: Text(
            'Buy THBD',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(0, 0, 0, 1),
                fontFamily: 'Segoe UI',
                fontSize: 20,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        Positioned(
          top: 90,
          left: 20,
          child: Container(
            width: 120.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 255, 255, 255),
              boxShadow: [
                BoxShadow(
                    color: Color.fromARGB(66, 255, 255, 255),
                    blurRadius: 6,
                    offset: Offset(0, 2))
              ],
            ),
            child: TextField(
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 20,
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: '0.00',
                hintStyle: TextStyle(
                  color: Colors.black38,
                ),
              ),
            ),
          ),
        ),
        // Positioned(
        //   top: 22,
        //   left: 219,
        //   child: Container(
        //     width: 26,
        //     height: 26,
        //     decoration: BoxDecoration(
        //       image: DecorationImage(
        //           image: AssetImage('assets/images/Monerocoin22151.png'),
        //           fit: BoxFit.fitWidth),
        //     ),
        //   ),
        // ),
        Positioned(
          top: 210,
          left: 17,
          child: Text(
            '1.00',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
                fontFamily: 'Segoe UI',
                fontSize: 25,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        Positioned(
          top: 90,
          left: 200,
          child: Container(
            width: 120.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 255, 255, 255),
              boxShadow: [
                BoxShadow(
                    color: Color.fromARGB(66, 255, 255, 255),
                    blurRadius: 6,
                    offset: Offset(0, 2))
              ],
            ),
            child: TextField(
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 20,
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: '0.00',
                hintStyle: TextStyle(
                  color: Colors.black38,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: 210,
          left: 260,
          child: Text(
            '30.00',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
                fontFamily: 'Segoe UI',
                fontSize: 25,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
          Positioned(
          top: 210,
          left: 155,
          child: Text(
            'To',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
                fontFamily: 'Segoe UI',
                fontSize: 20,
                letterSpacing:
                    0 /*percentages not used in flutter. defaulting to zero*/,
                fontWeight: FontWeight.normal,
                height: 1),
          ),
        ),
        // Positioned(
        //   top: 19,
        //   left: 271,
        //   child: Container(
        //     width: 33,
        //     height: 33,
        //     decoration: BoxDecoration(
        //       image: DecorationImage(
        //           image: AssetImage('assets/images/Bitcoin_png481.png'),
        //           fit: BoxFit.fitWidth),
        //     ),
        //   ),
        // ),
        Positioned(
          top: 85,
          left: 160,
          child: Container(
            width: 23,
            height: 23,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/transfer.png'),
                  fit: BoxFit.fitWidth),
            ),
          ),
        ),
      ],
    ),
  );
}
