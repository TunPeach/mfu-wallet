// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:myapp/page/low_bar/lowbar.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(180),
          child: AppBar(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
            ),
            centerTitle: true,
            leading: IconButton(
              onPressed: () {},
              icon: Icon(Icons.account_circle),
              iconSize: 35.0,
              padding: EdgeInsets.all(20.0),
            ),
            actions: [
              SizedBox(height: 20),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.notifications_none),
                iconSize: 35.0,
                padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
              ),
              SizedBox(height: 20),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.power_settings_new),
                iconSize: 35.0,
                padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
              ),
              SizedBox(height: 20),
            ],
            flexibleSpace: Container(
              child: Align(
                alignment: Alignment(0, 0.5),
                child: Text(
                  'History',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'SegoeUI_Bold',
                    fontSize: 50,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                ),
                gradient: LinearGradient(
                  colors: [
                    Color.fromARGB(255, 179, 0, 0),
                    Color.fromARGB(255, 132, 15, 15),
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomCenter,
                ),
                image: DecorationImage(
                  image: AssetImage("assets/mfu_icon.png"),
                  alignment: Alignment(0, -0.6),
                  scale: 7,
                ),
              ),
            ),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(10.0),
          child: ListView(
            children: <Widget>[
              
              history(),
            ],
          ),
        ));
  }

  Widget history() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: DataTable(
          columns: [
            DataColumn(label: Text('Servies')),
            DataColumn(label: Text('Amount')),
            DataColumn(label: Text('Date')),
          ],
          rows: [
            DataRow(cells: [
              DataCell(Text('disposit')),
              DataCell(Text('1000')),
              DataCell(Text('1/20/18')),
            ]),
            DataRow(cells: [
              DataCell(Text('transfer')),
              DataCell(Text('1000')),
              DataCell(Text('1/20/18')),
            ]),
            DataRow(cells: [
              DataCell(Text('withdraw')),
              DataCell(Text('1000')),
              DataCell(Text('1/20/18')),
            ]),
          ],
        ),
      ),
    );
  }
}
