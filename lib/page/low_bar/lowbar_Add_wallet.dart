// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:myapp/page/Add_Wallet.dart';
import 'package:myapp/page/History.dart';
import 'package:myapp/page/page_confirm/Payment_confirm.dart';
import 'package:myapp/page/scan.dart';
import '/page/Homepage.dart';
import '/page/more.dart';
import '/page/payment.dart';
import '/page/withdraw.dart';
import '/page/Exchange.dart';
import '/page/page_confirm/Transfer_confirm.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import '/page/balance.dart';
import 'lowbar_balance.dart';

class Home_wallet extends StatefulWidget {
  const Home_wallet({Key? key}) : super(key: key);

  @override
  _Home_walletState createState() => _Home_walletState();
}

class _Home_walletState extends State<Home_wallet> {
  int currentTab = 0;
  final List<Widget> screens = [
    Add_Wallet()
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = Add_Wallet();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: Image(image: AssetImage('assets/mfuIcon.png')),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => lowbar_balance()));
        },
        backgroundColor: Color.fromARGB(255, 99, 0, 0),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(10),
              topLeft: Radius.circular(10),
            ),
            gradient: LinearGradient(
              // ignore: prefer_const_literals_to_create_immutables
              colors: [
                Color.fromARGB(255, 132, 15, 15),
                Color.fromARGB(255, 99, 0, 0)
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomCenter,
            ),
          ),
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(
                        () {
                          currentScreen = Homepage();
                          currentTab = 0;
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: currentTab == 0 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'Home',
                          style: TextStyle(
                            color:
                                currentTab == 0 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () async {
                      ScanResult codeSanner =
                          await BarcodeScanner.scan(); //barcode scnner
                      setState(
                        () {
                          String qrResult = codeSanner.rawContent;
                          currentScreen =
                              Transfer_confirm(qrCodeResult: qrResult);
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.paid,
                          color: currentTab == 1 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'Transfer',
                          style: TextStyle(
                            color:
                                currentTab == 1 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
///////////////////////////////////////Right Tab Bar Icons/////////////////////////////////////////////////////////////////////
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () async {
                      ScanResult codeSanner =
                          await BarcodeScanner.scan(); //barcode scnner
                      setState(
                        () {
                          String qrResult = codeSanner.rawContent;
                          currentScreen =
                              Payment_confirm(qrCodeResult: qrResult);
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.qr_code,
                          color: currentTab == 2 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'ScanQR',
                          style: TextStyle(
                            color:
                                currentTab == 2 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(
                        () {
                          currentScreen = More();
                          currentTab = 2;
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.more_horiz,
                          color: currentTab == 1 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'More',
                          style: TextStyle(
                            color:
                                currentTab == 1 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
