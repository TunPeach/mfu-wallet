// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:myapp/page/page_confirm/Payment_confirm.dart';
import 'package:myapp/page/scan.dart';
import '../Homepage.dart';
import '../more.dart';
import '../payment.dart';
import '../withdraw.dart';
import '../Exchange.dart';
import '../page_confirm/Transfer_confirm.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import '../balance.dart';
import 'lowbar_balance.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentTab = 0;
  final List<Widget> screens = [
    Homepage(),
    Payment(),
    More(),
    Withdraw(),
    Exchange(),
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = Homepage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: Image(image: AssetImage('assets/mfuIcon.png')),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => lowbar_balance()));
        },
        backgroundColor: Color.fromARGB(255, 99, 0, 0),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(10),
              topLeft: Radius.circular(10),
            ),
            gradient: LinearGradient(
              // ignore: prefer_const_literals_to_create_immutables
              colors: [
                Color.fromARGB(255, 132, 15, 15),
                Color.fromARGB(255, 99, 0, 0)
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomCenter,
            ),
          ),
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(
                        () {
                          currentScreen = Homepage();
                          currentTab = 0;
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: currentTab == 0 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'Home',
                          style: TextStyle(
                            color:
                                currentTab == 0 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () async {
                      ScanResult codeSanner =
                          await BarcodeScanner.scan(); //barcode scnner
                      setState(
                        () {
                          String qrResult = codeSanner.rawContent;
                          currentScreen =
                              Transfer_confirm(qrCodeResult: qrResult);
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.paid,
                          color: currentTab == 1 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'Transfer',
                          style: TextStyle(
                            color:
                                currentTab == 1 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
///////////////////////////////////////Right Tab Bar Icons/////////////////////////////////////////////////////////////////////
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () async {
                      ScanResult codeSanner =
                          await BarcodeScanner.scan(); //barcode scnner
                      setState(
                        () {
                          String qrResult = codeSanner.rawContent;
                          currentScreen =
                              Payment_confirm(qrCodeResult: qrResult);
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.qr_code,
                          color: currentTab == 2 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'ScanQR',
                          style: TextStyle(
                            color:
                                currentTab == 2 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
////////////////////////////////////////////////////////////////////////////////////////
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(
                        () {
                          currentScreen = More();
                          currentTab = 2;
                        },
                      );
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.more_horiz,
                          color: currentTab == 1 ? Colors.white : Colors.white,
                        ),
                        Text(
                          'More',
                          style: TextStyle(
                            color:
                                currentTab == 1 ? Colors.white : Colors.white,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
