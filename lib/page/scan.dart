// ignore_for_file: prefer_const_constructors

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/material.dart';

class ScanPage extends StatefulWidget {
   String qrCodeResult;
   ScanPage({Key? key,required this.qrCodeResult});

  @override
  _ScanPageState createState() => _ScanPageState(qrCodeResult);

}

class _ScanPageState extends State<ScanPage> {
  var qrCodeResult;

  _ScanPageState(this.qrCodeResult);

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
appBar: PreferredSize(
        preferredSize: Size.fromHeight(180),
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.account_circle),
            iconSize: 35.0,
            padding: EdgeInsets.all(20.0),
          ),
          actions: [
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.power_settings_new),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
          ],
          flexibleSpace: Container(
            child: Align(
              alignment: Alignment(0, 0.5),
              child: Text(
                'More',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'SegoeUI_Bold',
                  fontSize: 50,
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
              gradient: LinearGradient(
                colors: [Color.fromARGB(255, 179, 0, 0), Color.fromARGB(255, 132, 15, 15),],
                begin: Alignment.topLeft,
                end: Alignment.bottomCenter,
              ),
              image: DecorationImage(
                image: AssetImage("assets/mfu_icon.png"),
                alignment: Alignment(0, -0.6),
                scale: 7,
              ),
            ),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              "Result",
              style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Text(
              qrCodeResult,
              style: TextStyle(
                fontSize: 20.0,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  //its quite simple as that you can use try and catch staatements too for platform exception
}
