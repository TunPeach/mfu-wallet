// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'Add_Wallet.dart';
import 'generate.dart';
import 'low_bar/lowbar_Add_wallet.dart';
import 'scan.dart';
import 'page_confirm/Transfer_confirm.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'Homepage.dart';
import 'low_bar/lowbar_profile.dart';
class More extends StatefulWidget {
  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
  Widget currentScreen = Homepage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(180),
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.account_circle),
            iconSize: 35.0,
            padding: EdgeInsets.all(20.0),
          ),
          actions: [
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.power_settings_new),
              iconSize: 35.0,
              padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
            ),
            SizedBox(height: 20),
          ],
          flexibleSpace: Container(
            child: Align(
              alignment: Alignment(0, 0.5),
              child: Text(
                'More',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'SegoeUI_Bold',
                  fontSize: 50,
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 179, 0, 0),
                  Color.fromARGB(255, 132, 15, 15),
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomCenter,
              ),
              image: DecorationImage(
                image: AssetImage("assets/mfu_icon.png"),
                alignment: Alignment(0, -0.6),
                scale: 7,
              ),
            ),
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            buttonProfile("Profile"),
            SizedBox(height: 20.0),
            buttonAddWallet("Add Wallet", Home_wallet()),
            SizedBox(height: 20.0),
          ],
        ),
      ),
    );
  }

  Widget buttonProfile(String text) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
          ),
          gradient: LinearGradient(
            // ignore: prefer_const_literals_to_create_immutables
            colors: [
              Color.fromARGB(255, 179, 0, 0),
              Color.fromARGB(255, 132, 15, 15),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
          ),
          image: DecorationImage(
            alignment: Alignment(-0, -0.5),
            scale: 5,
            image: AssetImage('assets/profile icon.png'),
          )),
      height: 200,
      width: 600,
      child: OutlinedButton(
        onPressed: () async {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => Home_profile()));
        },
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 120, 0, 0),
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontFamily: 'Trajan Pro',
            ),
          ),
        ),
        style: ElevatedButton.styleFrom(
          minimumSize: Size.fromHeight(80),
          textStyle: TextStyle(fontSize: 20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  Widget buttonAddWallet(
    String text,
    Widget widget,
  ) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
          ),
          gradient: LinearGradient(
            // ignore: prefer_const_literals_to_create_immutables
            colors: [
              Color.fromARGB(255, 179, 0, 0),
              Color.fromARGB(255, 132, 15, 15),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
          ),
          image: DecorationImage(
            alignment: Alignment(-0, -0.5),
            scale: 5,
            image: AssetImage('assets/wallet_white.png'),
          )),
      height: 200,
      width: 600,
      child: OutlinedButton(
        onPressed: () async {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => widget));
        },
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 120, 0, 0),
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontFamily: 'Trajan Pro',
            ),
          ),
        ),
        style: ElevatedButton.styleFrom(
          minimumSize: Size.fromHeight(80),
          textStyle: TextStyle(fontSize: 20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
